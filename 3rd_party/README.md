# 3rd-Party Components

This directory contains the following 3rd party components

## FreeSans.ttf

This is a font file distributed as part of [fonts-freefont-ttf][fonts-deb]
Debian package. It's been copyrighted by:
Copyright: 2002-2012, GNU Freefont contributors

And it is being distributed under the terms of [GPL-3.0][GPL-3.0] License.
For more details, please see the [copyright file][fonts-copyright]

[fonts-deb]: https://tracker.debian.org/fonts-freefont
[fonts-copyright]: https://sources.debian.org/src/fonts-freefont/20211204%2Bsvn4273-2/debian/copyright/
