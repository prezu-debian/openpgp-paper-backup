use std::error::Error;
use std::fmt::Formatter;
use std::sync::mpsc;
use std::sync::mpsc::Sender;
use std::{fmt, thread};

use image;
use image::{ImageBuffer, Luma};
use qrcode::QrCode;

const MIN_DIMENSION: u32 = 800;

/// Represents an error that occurred while processing a QR code.
#[derive(Debug)]
struct QrCodeError {
    msg: String,
}

impl fmt::Display for QrCodeError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.msg)
    }
}

impl Error for QrCodeError {}

/// Generates a QR code using the given input string and saves it to the
/// specified path.
///
/// # Arguments
///
/// * `input` - The input string to encode into the QR code. Maximum allowed
///  length of the input is 1620 characters.
/// * `path` - The path to save the generated QR code image.
///
/// # Returns
///
/// Returns [`Ok(image)`] if the QR code is successfully generated and saved to
/// the specified path. The `image` is the `image`'s crate representation of
/// the genrated QrCode image. The image has also been saved by the function to
/// the `path` location.
/// Returns [`Err`] if there is an error during the generation or saving of the
/// QR code.
///
/// # Examples
///
/// ```
/// use std::error::Error;
///
/// match generate_qr_code("Hello, World!", "./qr_code.bmp") {
///     Ok(_) => println!("QR code generated and saved successfully."),
///     Err(e) => eprintln!("Error generating QR code: {}", e),
/// }
/// ```
/// [`Ok(0)`]: Result
pub(super) fn generate_qr_code(
    input: &str,
    path: &str,
) -> Result<ImageBuffer<Luma<u8>, Vec<u8>>, Box<dyn Error>> {
    if input.len() > 1620 {
        let err_msg = concat!(
            "Input too long! Max allowed length is ",
            "1620 characters."
        )
        .to_string();
        return Err(Box::new(QrCodeError { msg: err_msg }));
    }
    let code = QrCode::new(input)?;
    let png = code
        .render::<Luma<u8>>()
        .min_dimensions(MIN_DIMENSION, MIN_DIMENSION)
        .build();
    png.save(path)?;
    Ok(png)
}

/// This function abstracts running Qr Code detection in a separate thread. It's meant to be
/// executed from within a specific thread's context.
/// It accepts the following parameters:
/// - `qr_decoding_impl`: A specific implementation we want to run in the thread. It's either
///  `decode_qr_code_zbar` or `decode_qr_code_rqrr`.
/// - `qr_code_file_path`: A `String` representing the path to the QR code file.
/// - `tx`: A reference to a `Sender<Result<Vec<String>, String>>` to send the decoding result or
///  error message back to the main thread.
/// - `impl_id`: A `String` representing the identifier of the implementation. It's either `zbar`
///  or `rqrr`.
/// ```
fn qr_code_decoding_closure<F>(
    qr_decoding_impl: F,
    qr_code_file_path: String,
    tx: &Sender<Result<Vec<String>, String>>,
    impl_id: String,
) where
    F: Fn(String) -> Result<Vec<String>, Box<dyn Error>>,
{
    let res = qr_decoding_impl(qr_code_file_path);
    let err_msg = match res {
        Ok(codes) => {
            tx.send(Ok(codes)).expect(
                format!(
                    "Failed to send {} result back to the main thread",
                    impl_id
                )
                .as_str(),
            );
            None
        }
        Err(e) => Some(e.to_string()),
    };
    if err_msg.is_some() {
        let err_msg = err_msg.unwrap();
        tx.send(Err(err_msg))
            .expect(
                format!(
                    "Failed to propagate error from the {} thread back to the mail thread", impl_id
                ).as_str()
            );
    };
}

/// Decode QR code from an image file.
///
/// # Arguments
///
/// * `image_path` - The path to the image file.
///
/// # Returns
///
/// Returns a `Result` containing the decoded QR code as a `String`.
/// If decoding is successful, the `String` value is returned inside an `Ok`
/// variant. If an error occurs during decoding, an `Err` variant containing a
/// `Box<dyn Error>` is returned. The `Box<dyn Error>` represents the type of
/// error that occurred during the decoding process.
///
/// # Examples
///
/// ```
/// use std::error::Error;
///
/// fn main() -> Result<(), Box<dyn Error>> {
///     let image_path = "path/to/image.png";
///     let decoded_qr_codes = decode_qr_code(image_path)?;
///     for code in decoded_qr_codes {
///         println!("Decoded QR code: {}", code);
///     }
///     Ok(())
/// }
/// ```
pub(super) fn decode_qr_code(
    image_path: String,
) -> Result<Vec<String>, Box<dyn Error>> {
    let image_path_zbar_copy = image_path.clone();
    let image_path_rqrr_copy = image_path.clone();
    let (tx_zbar, rx_zbar) = mpsc::channel();
    let (tx_rqrr, rx_rqrr) = mpsc::channel();
    let zbar_handle = thread::spawn(move || {
        qr_code_decoding_closure(
            decode_qr_code_zbar,
            image_path_zbar_copy,
            &tx_zbar,
            "zbar".to_string(),
        )
    });

    let rqrr_handle = thread::spawn(move || {
        qr_code_decoding_closure(
            decode_qr_code_rqrr,
            image_path_rqrr_copy,
            &tx_rqrr,
            "rqrr".to_string(),
        )
    });

    // let zbar_result = decode_qr_code_zbar(image_path);
    zbar_handle
        .join()
        .expect("Failed to execute zbar detection thread");
    rqrr_handle
        .join()
        .expect("Failed to execute rqrr detection thread");
    let zbar_result = rx_zbar
        .recv()
        .expect("Failed to receive results from zbar thread");
    let rqrr_result = rx_rqrr
        .recv()
        .expect("Failed to receive results from rqrr thread");

    // If both succeed, favour the one with more codes
    if zbar_result.is_ok() && rqrr_result.is_ok() {
        let zbar_codes = zbar_result.unwrap();
        let rqrr_codes = rqrr_result.unwrap();
        if rqrr_codes.len() > zbar_codes.len() {
            info!("Both, zbar and rqrr, yielded correct results. Rqrr returned more codes.");
            return Ok(rqrr_codes);
        }
        info!("Both, zbar and rqrr, yielded correct results. Zbar returned more codes.");
        return Ok(zbar_codes);
    };

    if zbar_result.is_ok() {
        warn!("Only zbar yielded correct result for {}", image_path);
        return Ok(zbar_result?);
    }
    if rqrr_result.is_ok() {
        warn!("Only rqrr yielded correct result for {}", image_path);
        return Ok(rqrr_result?);
    }
    let err_msg = format!(
        "Zbar failed with: {:?}, rqrr failed with: {:?}",
        zbar_result, rqrr_result
    );
    error!("Both, zbar and rqrr failed to process {}", image_path);
    Err(Box::new(QrCodeError { msg: err_msg }))
}

fn decode_qr_code_zbar(
    image_path: String,
) -> Result<Vec<String>, Box<dyn Error>> {
    use image::GenericImageView;
    use zbar_rust::ZBarImageScanner;

    let mut codes: Vec<String> = Vec::new();

    let img = image::open(image_path)?;
    let (width, height) = img.dimensions();
    let mut scanner = ZBarImageScanner::new();
    let results = scanner
        .scan_y800(img.into_luma8().into_raw(), width, height)
        .unwrap();
    for result in results {
        // println!("{}", String::from_utf8(result.data).unwrap())
        codes.push(String::from_utf8(result.data)?);
    }
    Ok(codes)
}

fn decode_qr_code_rqrr(
    image_path: String,
) -> Result<Vec<String>, Box<dyn Error>> {
    let image = image::open(image_path)?.to_luma8();
    let mut img = rqrr::PreparedImage::prepare(image);
    let grids = img.detect_grids();
    // assert_ne!(grids.len(), 0);
    let mut codes = Vec::<String>::new();
    for grid in grids {
        let (meta, content) = grid.decode()?;
        assert_eq!(meta.ecc_level, 0);
        codes.push(content);
    }
    Ok(codes)
}

#[cfg(test)]
mod tests {
    use std::collections::BTreeSet;
    use std::path::Path;
    use std::string::String;

    use super::*;

    fn generate_long_text() -> String {
        let chars: Vec<char> = "abcdefghijklmnopqrstuvwxyz".chars().collect();
        let mut long_input_vec: Vec<char> = Vec::new();
        for i in 0..1620 {
            long_input_vec.push(chars[i % chars.len()]);
        }
        // We return: "abc...xyzabc..." long string (total of 1620
        // characters)
        long_input_vec.iter().collect()
    }

    /// Test that `generate_qr_code()` function creates a BMP file.
    #[test]
    fn test_generate_qr_code_creates_image_file() {
        // Given
        let text = "Patryk Cisek is testing QrCode generation.";
        for ext in ["png", "jpeg"] {
            let path = format!("test_1.{}", ext);

            // When
            generate_qr_code(text, path.as_str()).unwrap();

            // Then
            assert!(Path::new(path.as_str()).exists());
            std::fs::remove_file(path).unwrap();
        }
    }

    /// Test that generating and decoding a QR code results in the correct
    /// text.
    #[test]
    fn test_generate_and_decode_roundtrip_returns_proper_results() {
        // Given
        let text = concat!(
            "I'm making sure, the roundtrip: generating a QrCode from a ",
            "known text and later decoding it back into a string."
        );
        for ext in ["png", "jpg"] {
            let qr_code_file_path = format!("qr_code.{}", ext);
            generate_qr_code(text, qr_code_file_path.as_str()).unwrap();

            // When
            let decoded_codes =
                decode_qr_code(qr_code_file_path.to_owned()).unwrap();
            std::fs::remove_file(qr_code_file_path.as_str()).expect(
                format!("Failed to remove {}", qr_code_file_path).as_str(),
            );
            assert_eq!(decoded_codes.len(), 1);
            assert_eq!(decoded_codes[0], text);
        }
    }

    /// Test that we can generate QrCode from a long (2329 characters) ASCII
    /// string.
    #[test]
    fn test_generate_succeeds_for_long_input() {
        // Given
        for ext in ["png", "jpg"] {
            let path = format!("qr_code_2.{}", ext);
            let long_input = generate_long_text();
            let expected_width = 300;
            let expected_height = 300;

            // When
            let result =
                generate_qr_code(long_input.as_str(), path.as_str()).unwrap();
            std::fs::remove_file(path).expect("Failed to delete test file");
            assert!(result.height() >= expected_height);
            assert!(result.width() >= expected_width);
        }
    }

    /// Test that very long string can be coded and later decoded correctly.
    #[test]
    fn test_very_long_string_roundtrip_encoding_decoding_works_correctly() {
        // Given
        for ext in ["png", "jpeg"] {
            let path = format!("qr_code_3.{}", ext);
            let long_input = generate_long_text();
            generate_qr_code(long_input.as_str(), path.as_str()).unwrap();

            // When
            let results = decode_qr_code(path.to_owned()).unwrap();

            // Then
            std::fs::remove_file(path).expect("Failed to delete test file");
            assert_eq!(results.len(), 1);
            assert_eq!(results[0], long_input);
        }
    }

    /// Test that scanned image is detected correctly.
    #[test]
    fn test_scanned_image_is_detected_correctly() {
        // Given
        let path = "tests/test_files/qr_codes/scanned_1.png".to_string();
        let expected_code = String::from(concat!(
            "I'm making sure, the roundtrip: generating a QrCode from a ",
            "known text and later decoding it back into a string."
        ));

        // When
        let results = decode_qr_code(path).unwrap();

        // Then
        assert_eq!(results.len(), 1);
        assert_eq!(results[0], expected_code);
    }

    /// Test that scanned QrCode surrounded by text is detected correctly.
    #[test]
    fn test_scanned_qrcode_surrounded_by_text_detected_correctly() {
        // Given
        let path = "tests/test_files/qr_codes/scanned_2.png".to_string();
        let expected_code =
            String::from("Patryk Cisek is testing QrCode generation.");

        // When
        let results = decode_qr_code(path).unwrap();

        // Then
        assert_eq!(results.len(), 1);
        assert_eq!(results[0], expected_code);
    }

    /// Test that string longer than 1620 characters will fail to encode.
    #[test]
    fn test_too_long_text_fails_to_encode_to_qr_code() {
        // Given
        let too_long_input: String =
            std::iter::repeat("a").take(1621).collect();
        assert_eq!(too_long_input.len(), 1621);

        // When
        let err = generate_qr_code(too_long_input.as_str(), "qr_code_4.png");

        // Then
        assert!(err.is_err());
    }

    /// Test that when we have a scan of a page with multiple QrCodes
    /// surrounded by random text are still detected correctly.
    #[test]
    fn test_multiple_qr_codes_with_text_decode_properly_long() {
        // Given
        let inputs = vec![
            "default canola bungee endocrine flame science liftoff powdered",
            "antibody diner hatchback sensitive payday unaudited scorn",
            "resisting onboard pester path deduct crisped bonnet vessel",
            "knoll underfeed smartness reunion caddie evidence playgroup",
            "patient earlobe operative aviator vascular cornflake electable",
            "duchess craving garden stifling boots crept cramp factoid",
        ];
        let sorted_inputs =
            BTreeSet::from_iter(inputs.iter().map(|&s| String::from(s)));
        let path = concat!(
            "tests/test_files/qr_codes/",
            "scanned_multiple_qr_codes_with_text.jpeg"
        )
        .to_string();

        // When
        let codes = decode_qr_code(path).unwrap();

        // Then
        let sorted_codes = BTreeSet::from_iter(codes);
        assert_eq!(sorted_codes, sorted_inputs);
    }

    #[test]
    fn test_very_short_text_produces_larger_qr_code() {
        // Given
        let path = "qr_code_5.jpeg";

        // When
        let result = generate_qr_code("a", path).unwrap();

        // Then
        std::fs::remove_file(path).expect("Failed to delete test file");
        assert!(result.height() >= MIN_DIMENSION);
        assert!(result.width() >= MIN_DIMENSION);
    }
}
