use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::Path;
use std::str::FromStr;
use std::string::String;

use ::image::io::Reader as ImageReader;
use printpdf::*;
use sequoia_openpgp::armor::{Kind, Writer};
use sequoia_openpgp::parse::Parse;
use sequoia_openpgp::serialize::Serialize;
use sequoia_openpgp::Cert;
use tempfile::NamedTempFile;

use crate::qr_code::{decode_qr_code, generate_qr_code};

pub(crate) struct BackupCtx {
    pub(crate) backup_chunk_size: usize,
}

fn user_info_from_cert(cert: &Cert) -> Result<Vec<String>, Box<dyn Error>> {
    let mut output = Vec::<String>::new();
    output.push(format!(
        "Certificate's fingerprint: {}",
        cert.fingerprint().to_string()
    ));
    output.push(String::from(format!(
        "Certificate contains {} User IDs:\n",
        cert.userids().len()
    )));

    for uid in cert.userids() {
        let uid = uid.userid();
        let mut uid_str = String::new();
        if let Some(name) = uid.name2()? {
            uid_str = name.to_string();
        }
        if let Some(email) = uid.email2()? {
            uid_str += &format!(" <{}>", email);
        }
        output.push(uid_str);
    }
    Ok(output)
}

/// Inserts an image onto a page of a PDF document.
///
/// # Arguments
///
/// * `image_path` - The path to the image file.
/// * `current_layer` - The reference to the current layer of the PDF document.
/// * `pos_y` - The Y-axis position of the image on the page.
///
/// # Returns
///
/// Returns `Ok(())` if the image is inserted successfully
/// or an error of type `Box<dyn Error>` if an error occurs.
fn insert_image_on_page(
    image_path: &Path,
    current_layer: &PdfLayerReference,
    pos_y: f32,
) -> Result<(), Box<dyn Error>> {
    let dynamic_image = ImageReader::open(image_path)?.decode()?;
    let image = Image {
        image: ImageXObject::from_dynamic_image(&dynamic_image),
    };
    image.add_to_layer(
        current_layer.clone(),
        ImageTransform {
            translate_x: Some(Mm(50.0)),
            translate_y: Some(Mm(pos_y)),
            ..Default::default()
        },
    );
    Ok(())
}

/// Generates a QR Code and returns the path where it is saved.
///
/// # Arguments
///
/// * `dest_dir` - The destination directory where the QR code will be saved.
/// * `base_name` - The base name of the QR code file.
/// * `index` - The index of the QR code.
/// * `chunk` - The chunk of data to be encoded in the QR code.
///
/// # Returns
///
/// The path of the generated QR code.
fn generate_and_return_qr_code_path(
    dest_dir: &str,
    base_name: &str,
    index: usize,
    chunk: &str,
) -> String {
    let qr_code_path =
        format!("{}/{}_{:02}.jpeg", dest_dir, base_name, index);
    let indexed_chunk = format!("{:02}\n{}", index, chunk);

    generate_qr_code(&indexed_chunk, &qr_code_path)
        .expect(&format!("Failed to generate Qr Code: {}", qr_code_path));

    qr_code_path
}

/// Takes a vector of strings and regroups them into batches of a specified size.
///
/// # Arguments
///
/// * `strings` - A reference to a vector of strings.
/// * `batch_size` - The size of each batch.
///
/// # Returns
///
/// A vector of vectors of strings, where each inner vector represents a batch of strings.
///
/// # Example
///
/// ```
/// let strings = vec!["hello", "world", "rust", "doc"];
/// let batch_size = 2;
/// let result = regroup_strings_in_batches(&strings, batch_size);
/// assert_eq!(result, vec![vec!["hello", "world"], vec!["rust", "doc"]]);
/// ```
fn regroup_strings_in_batches(
    strings: &Vec<String>,
    batch_size: usize,
) -> Vec<Vec<String>> {
    let mut batches: Vec<Vec<String>> = Vec::new();
    let mut batch: Vec<String> = Vec::new();
    for inp in strings {
        batch.push(inp.to_owned());
        if batch.len() == batch_size {
            batches.push(batch);
            batch = Vec::new();
        }
    }
    if batch.len() > 0 {
        batches.push(batch);
    }
    batches
}

/// This function takes batches of QR codes and adds them into a PDF document.
///
/// ## Parameters
/// - `batches`: A collection of QR Code file names in batch. Each batch will be placed in on a
///  single page.
/// - `document`: The PDF document to which the QR Codes are to be added.
///
/// ## Returns
/// This function returns a Result type. If the operation is successful, it returns the updated PDF
/// document with added QR Codes.
/// If the operation fails, it will return an error of type `std::io::Error`.
///
/// ## Panics
/// This function will panic if any of the QR codes in the batches are invalid.
///
/// ## Errors
/// This function will return an error if the PDF document cannot be opened or written to.
///
/// ## Safety
/// This function is not marked as `unsafe`, thus there are no specific safety considerations the caller must adhere to.
fn add_qr_code_batches_to_pdf(
    doc: &PdfDocumentReference,
    font: &IndirectFontRef,
    batches: &Vec<Vec<String>>,
) -> Result<(), Box<dyn Error>> {
    for batch in batches {
        let mut pos_y = 140f32;
        let (new_page, layer) = doc.add_page(Mm(210.0), Mm(247.0), "page");
        let current_layer = doc.get_page(new_page).get_layer(layer);

        for path in batch {
            let path = Path::new(&path);
            insert_image_on_page(&path, &current_layer, pos_y)?;
            let filename = path
                .file_name()
                .expect("Failed to extract Qr Code filename");
            let filename =
                filename.to_str().expect("Failed to convert OsStr to str");
            current_layer.use_text(
                filename,
                12.0,
                Mm(52.5),
                Mm(pos_y - 5.0),
                &font,
            );
            pos_y -= 102f32;
        }
    }
    Ok(())
}

/// Serialize a `Cert` object to a PEM-formatted text representation.
///
/// # Arguments
///
/// * `cert` - A reference to the `Cert` object to serialize.
///
/// # Returns
///
/// A `Result` containing a `String` if the serialization is successful, or a
/// `Box<dyn Error>` if an error occurred during the conversion.
///
/// # Errors
///
/// The function can return an error if any of the following conditions are
/// met:
///
/// * The `cert` object does not have valid armor headers (missing headers are
///  fine).
/// * The serialization of the `cert` object fails.
///
/// # Examples
///
/// ```rust
/// use std::error::Error;
///
/// fn main() -> Result<(), Box<dyn Error>> {
///     let cert = Cert::new();
///     let text = cert_to_text(&cert)?;
///     println!("{}", text);
///     Ok(())
/// }
/// ```
///
fn cert_to_text(cert: &Cert) -> Result<String, Box<dyn Error>> {
    let headers = cert.armor_headers();
    let mut buf: Vec<u8> = Vec::new();
    let headers: Vec<_> =
        headers.iter().map(|h| ("Comment", h.as_str())).collect();
    let mut writer =
        Writer::with_headers(&mut buf, Kind::SecretKey, headers)?;
    cert.as_tsk().serialize(&mut writer)?;
    writer.finalize()?;
    let result_str = String::from_utf8(buf)?;
    Ok(result_str)
}

/// Splits a given text into chunks of a specified size and returns them as a
/// collection of strings.
///
/// # Arguments
///
/// * `input_text` - A string slice representing the text to be split.
/// * `chunk_size` - A usize value representing the chunk size
///
/// # Returns
///
/// A vector of strings, where each string is a chunk of the given text.
fn split_text_into_chunks(
    input_text: &str,
    chunk_size: usize,
) -> Vec<String> {
    let mut idx = 0;
    let mut result = Vec::<String>::new();
    while idx < input_text.len() {
        let end_idx = std::cmp::min(idx + chunk_size, input_text.len());
        result.push(input_text[idx..end_idx].into());
        idx = end_idx;
    }
    result
}

/// Prints debug information about a given key.
///
/// # Arguments
///
/// * `key` - A reference to a `Cert` object representing the key.
///
/// # Errors
///
/// Returns a `Result` indicating success or failure. Failure occurs if there is an error while
/// retrieving the user IDs associated with the key.
///
/// # Example
///
/// ```rust
/// use std::error::Error;
/// use Cert;
///
/// fn main() -> Result<(), Box<dyn Error>> {
///     let key = Cert::new();
///     key_debug_info(&key)?;
///     Ok(())
/// }
/// ```
fn key_debug_info(key: &Cert) -> Result<(), Box<dyn Error>> {
    trace!("User IDs:");
    for header in key.armor_headers() {
        println!("{}", header);
    }
    for user_id in key.userids() {
        let uid = user_id.email2()?;
        match uid {
            Some(id) => {
                trace!("\tUser ID: {:?}", id);
            }
            _ => {}
        }
    }

    info!("The key contains {} keys", key.keys().count());

    for k in key.keys() {
        trace!("\tThe key: {:?}", k.fingerprint());
    }
    trace!("The key: {}", key);
    Ok(())
}

impl BackupCtx {
    /// # Backup Private Key
    ///
    /// This function is used to back up a private key to a specified directory with
    /// a given base name. The resulting backup will be a PDF file ready to be
    /// printed out. The PDF will contain the actual backup in the form of a
    /// series of QrCodes as well as an actual instruction on how to recover from
    /// the backup.
    ///
    /// ## Parameters
    ///
    /// - `base_name` - A `String` representing the base name of the backup file.
    /// - `backup_dir` - A `String` representing the directory where the backup
    ///  file will be stored.
    /// - `private_key` - A `String` representing the path to the private key file
    ///  to be backed up.
    ///
    /// ## Returns
    ///
    /// This function returns a `Result` indicating whether the backup was
    /// successful or if an error occurred.
    ///
    /// - `Ok(())` - If the backup was successful.
    /// - `Err(Box<dyn Error>)` - If an error occurred during the backup process.
    pub(super) fn backup(
        &self,
        base_name: String,
        backup_dir: String,
        private_key: String,
    ) -> Result<(), Box<dyn Error>> {
        println!(
            "Backing up Private Key: {} to directory: {} with the \
            base name: {}",
            private_key, backup_dir, base_name
        );
        let key = Cert::from_file(private_key.to_owned())?;

        let fpr = key.fingerprint();
        info!("Backing up a Private Key: {}", fpr);

        key_debug_info(&key)?;
        if !Path::new(&backup_dir).exists() {
            debug!("Creating the directory for QrCodes: {}", backup_dir);
            fs::create_dir_all(&backup_dir)?;
        }
        self.cert_to_pdf(&key, &base_name, &backup_dir)?;
        println!("Stored the backup in {} directory.", backup_dir);

        Ok(())
    }

    /// Convert a certificate to a PDF file for backup.
    ///
    /// # Arguments
    ///
    /// * `cert` - The certificate to convert to PDF.
    /// * `base_name` - The base name of the PDF file. E.g: `my_base` would result in `my_base.pdf`
    ///  being generated.
    /// * `dest_dir` - The destination directory for the PDF file.
    ///
    /// # Errors
    ///
    /// Returns an error if there is an issue with generating the PDF file.
    ///
    /// # Example
    ///
    /// ```rust
    /// use std::error::Error;
    ///
    /// // Create a sample certificate
    /// let cert = Cert::new();
    ///
    /// // Convert the certificate to PDF
    /// let base_name = "backup";
    /// let dest_dir = "/path/to/backup";
    /// let result = cert_to_pdf(&cert, base_name, dest_dir);
    /// assert!(result.is_ok());
    /// ```
    fn cert_to_pdf(
        &self,
        cert: &Cert,
        base_name: &str,
        dest_dir: &str,
    ) -> Result<(), Box<dyn Error>> {
        info!("About to save to PDF at: {}/{}.pdf", dest_dir, base_name);
        let user_info = user_info_from_cert(&cert)?;
        info!("User info:\n{:?}", user_info);
        let font_file = NamedTempFile::new()?;
        let font_path = font_file.path();
        fs::write(&font_path, include_bytes!("../3rd_party/FreeSans.ttf"))?;

        let (doc, page1, layer1) = PdfDocument::new(
            "OpenPGP Certificate Paper Backup",
            Mm(210.0),
            Mm(247.0),
            "Layer 2",
        );

        let current_layer = doc.get_page(page1).get_layer(layer1);
        let font = doc.add_external_font(File::open(&font_path)?)?;

        // Add title
        current_layer.use_text(
            "OpenPGP Paper Backup",
            40.0,
            Mm(30.0),
            Mm(220.0),
            &font,
        );
        for (idx, line) in user_info.iter().enumerate() {
            let y_pos = (200 - 5 * idx) as f32;
            if y_pos <= 30f32 {
                current_layer.use_text(
                    "...",
                    12.0,
                    Mm(20.0),
                    Mm(y_pos),
                    &font,
                );
                break;
            }
            current_layer.use_text(line, 12.0, Mm(20.0), Mm(y_pos), &font);
        }

        let cert_text_chunks = split_text_into_chunks(
            &cert_to_text(&cert)?,
            self.backup_chunk_size,
        );

        let qr_code_files: Vec<String> = cert_text_chunks
            .iter()
            .enumerate()
            .map(|(index, chunk)| {
                debug!("Generating Qr Code for index {}", index);
                generate_and_return_qr_code_path(
                    &dest_dir, &base_name, index, chunk,
                )
            })
            .collect();
        info!("Generated {} Qr Code files.", qr_code_files.len());
        let qr_code_batches = regroup_strings_in_batches(&qr_code_files, 2);
        info!("Generated {} batches", qr_code_batches.len());
        add_qr_code_batches_to_pdf(&doc, &font, &qr_code_batches)?;

        let full_pdf_path = format!("{}/{}.pdf", &dest_dir, &base_name);
        doc.save(&mut BufWriter::new(File::create(full_pdf_path)?))?;

        Ok(())
    }
}

fn read_dir_to_vec(dir_name: String) -> Result<Vec<String>, Box<dyn Error>> {
    let paths = fs::read_dir(dir_name)?;
    let mut result: Vec<String> = Vec::new();
    for entry in paths {
        let p = entry?.path();
        let p = p.to_str();
        match p {
            Some(str_path) => {
                result.push(str_path.to_string());
            }
            None => {
                panic!("Failed to convert path to string!");
            }
        }
    }
    Ok(result)
}

fn decoded_chunks_to_map(
    decoded_chunks: &Vec<String>,
) -> HashMap<usize, String> {
    let mut map: HashMap<usize, String> = HashMap::new();
    for chunk in decoded_chunks {
        let mut lines = chunk.lines();
        let idx_str = match lines.next() {
            None => {
                panic!("Failed to read an index of a chunk!");
            }
            Some(idx_str) => idx_str,
        };
        let idx = idx_str.parse().expect("Failed to extract chunk index");
        let new_line_len = "\n".len();
        let chunk: String =
            chunk[new_line_len + idx_str.len()..chunk.len()].to_string();
        info!("Restored chunk: {}", idx);
        map.insert(idx, chunk);
    }
    map
}

pub(super) fn restore(
    backup_dir: String,
    private_key: String,
) -> Result<(), Box<dyn Error>> {
    println!(
        "Attempting to restore backed-up key from directory: {} to file {}",
        backup_dir, private_key
    );
    let mut restored_cert_file = File::create(private_key)?;

    let qr_code_paths = read_dir_to_vec(backup_dir)?;
    let mut map: HashMap<usize, String> = HashMap::new();
    for p in qr_code_paths {
        info!("Processing chunk in {} ...", p);
        let decoded_chunks = decode_qr_code(p)?;
        map.extend(decoded_chunks_to_map(&decoded_chunks));
    }
    let mut restored_cert_str = String::new();
    for i in 0..map.len() {
        restored_cert_str += match map.get(&i) {
            Some(chunk) => chunk,
            None => panic!("Can't find a chunk with index: {}!!", i),
        };
    }
    let cert_str = Cert::from_str(&restored_cert_str.as_str())?;
    let restored_cert = cert_to_text(&cert_str)?;
    restored_cert_file.write_all(restored_cert.as_bytes())?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use std::fs;
    use std::path::Path;

    use sequoia_openpgp::parse::Parse;
    use sequoia_openpgp::Cert;

    use crate::backup_restore::{cert_to_text, split_text_into_chunks};
    use crate::backup_restore::{
        regroup_strings_in_batches, restore, BackupCtx,
    };

    fn create_bup_dir_and_load_cert(
        bup_dir: &str,
        cert_filename: &str,
    ) -> Cert {
        fs::create_dir_all(bup_dir)
            .expect(&format!("Failed to create test dir: {}", bup_dir));
        Cert::from_file(cert_filename)
            .expect(&format!("Failed to load cert from: {}", &cert_filename))
    }

    #[test]
    fn test_cert_to_pdf_returns_ok_for_valid_cert() {
        // Given
        let bup_dir = "01_cert_to_pdf_test";
        let base_name = "john_doe";
        let cert = create_bup_dir_and_load_cert(
            &bup_dir,
            "tests/test_files/example_keys/email_and_name/john.doe.pgp",
        );
        let ctx = BackupCtx {
            backup_chunk_size: 600,
        };

        // When
        let result = ctx.cert_to_pdf(&cert, &base_name, bup_dir);

        // Then
        fs::remove_dir_all(bup_dir).expect(&format!(
            "Failed to remove the test directory: {}",
            bup_dir
        ));
        assert!(result.is_ok());
    }

    #[test]
    fn test_cert_to_pdf_creates_pdf_in_bup_dir() {
        // Given
        let bup_dir = "02_cert_to_pdf_test";
        let base_name = "john_doe_backup_base";
        let expected_result_file = format!("{}/{}.pdf", bup_dir, base_name);
        let cert = create_bup_dir_and_load_cert(
            &bup_dir,
            "tests/test_files/example_keys/email_and_name/john.doe.pgp",
        );
        let ctx = BackupCtx {
            backup_chunk_size: 600,
        };

        // When
        let _ = ctx.cert_to_pdf(&cert, &base_name, &bup_dir);

        // Then
        let pdf_exists = Path::new(&expected_result_file).exists();
        fs::remove_dir_all(&bup_dir).expect(&expected_result_file);
        assert!(pdf_exists);
    }

    #[test]
    fn test_to_pdf_creates_expected_num_of_qr_codes_long() {
        // Given
        let bup_dir = "03_cert_to_pdf_test";
        let base_name = "john";
        let cert_file =
            "tests/test_files/example_keys/many_user_ids_key/cert.pgp";
        let cert = create_bup_dir_and_load_cert(&bup_dir, &cert_file);
        let serialized_cert = cert_to_text(&cert).expect(&format!(
            "Failed to convert the test cert {} to text",
            &cert_file
        ));
        let ctx = BackupCtx {
            backup_chunk_size: 600,
        };
        let e = serialized_cert.len() % ctx.backup_chunk_size > 0;
        let mut expected_chunks_count =
            serialized_cert.len() / ctx.backup_chunk_size;
        if e {
            expected_chunks_count = expected_chunks_count + 1;
        }

        // When
        let _ = ctx.cert_to_pdf(&cert, &base_name, &bup_dir);

        // Then
        let bup_dir_entries = fs::read_dir(&bup_dir).expect(&format!(
            "Failed to inspect test directory: {}",
            bup_dir
        ));
        let mut jpeg_count = 0;
        for entry in bup_dir_entries {
            let entry = entry.unwrap();
            if let Some(ext) = entry.path().extension() {
                if ext == "jpeg" {
                    jpeg_count += 1;
                }
            }
        }
        fs::remove_dir_all(&bup_dir).expect(&format!(
            "Failed to remove the test directory: {}",
            bup_dir
        ));
        assert_eq!(jpeg_count, expected_chunks_count);
    }

    #[test]
    fn test_cert_to_text_reproduces_the_orig_cert_file() {
        // Given
        let path = "tests/test_files/example_keys/email_only/example_key.pgp";
        let orig_key = fs::read_to_string(path).unwrap();
        let cert = Cert::from_file(path).unwrap();

        // When
        let serialized = cert_to_text(&cert).unwrap();

        // Then
        assert_eq!(serialized, orig_key);
    }
    #[test]

    fn test_cert_to_text_can_be_used_when_injesting_binary_key_file() {
        // Given
        let path = concat!(
            "tests/test_files/example_keys/example_key_bin_with_data/",
            "example_key_binary.pgp"
        );
        let cert = Cert::from_file(path).unwrap();

        // When
        let serialized = cert_to_text(&cert).unwrap();

        // Then
        let deserialized_cert =
            Cert::from_bytes(&serialized.into_bytes()).unwrap();
        assert_eq!(deserialized_cert, cert);
    }

    #[test]
    fn text_to_collection_returns_reconstructable_key() {
        // Given
        let cert = Cert::from_file(
            "tests/test_files/example_keys/email_only/example_key.pgp",
        )
        .unwrap();
        let chunk_size = 1000;
        let serialized = cert_to_text(&cert).unwrap();
        let in_chunks = split_text_into_chunks(&serialized, chunk_size);

        // When
        let reconstructed = in_chunks.join("");

        // Then
        assert_eq!(reconstructed, serialized);
    }

    #[test]
    fn test_text_to_collection_returns_2_elems_from_1200_long_text() {
        // Given
        let chunk_size = 1000;
        let text = "a".repeat(1200);

        // When
        let result = split_text_into_chunks(&text, chunk_size);

        // Then
        assert_eq!(2, result.len());
        assert_eq!(1000, result[0].len());
        assert_eq!(200, result[1].len());
    }

    /// Validate that an odd number of strings is properly regrouped
    #[test]
    fn regroup_strings_in_batches_for_odd_str_count_returns_correct_result() {
        // Given
        let input = vec![
            "One".to_string(),
            "Two".to_string(),
            "Three".to_string(),
            "Four".to_string(),
        ];
        let expected_result = vec![
            vec!["One".to_string(), "Two".to_string()],
            vec!["Three".to_string(), "Four".to_string()],
        ];

        // When
        let result = regroup_strings_in_batches(&input, 2);

        // Then
        assert_eq!(result, expected_result);
    }

    /// Validate that an even number of strings is properly regrouped
    #[test]
    fn regroup_strings_in_batches_for_even_str_count_returns_correct_result()
    {
        // Given
        let input = vec![
            "One".to_string(),
            "Two".to_string(),
            "Three".to_string(),
            "Four".to_string(),
            "Five".to_string(),
        ];
        let expected_result = vec![
            vec!["One".to_string(), "Two".to_string()],
            vec!["Three".to_string(), "Four".to_string()],
            vec!["Five".to_string()],
        ];

        // When
        let result = regroup_strings_in_batches(&input, 2);

        // Then
        assert_eq!(result, expected_result);
    }

    /// This test case checks if the `backup()` function returns an error when
    /// a non-OpenPGP file is provided.
    #[test]
    fn backup_returns_error_when_non_pem_file() {
        // Given
        let non_pem_file = "tests/test_files/various_files/text_file.pgp";
        let ctx = BackupCtx {
            backup_chunk_size: 600,
        };

        // When
        let result = ctx.backup(
            "abc".into(),
            "01_backup_dir".into(),
            non_pem_file.into(),
        );

        // Then
        println!("{:?}", result);
        assert!(result.is_err());
    }

    /// Validates that when backing-up a valid key, Ok(_) is being returned by
    /// the `backup()` function.
    #[test]
    fn backup_returns_ok_on_success() {
        // Given
        let example_key_file =
            "tests/test_files/example_keys/email_only/example_key.pgp";
        let bup_dir = "02_backup_dir";
        let ctx = BackupCtx {
            backup_chunk_size: 600,
        };

        // When
        let result =
            ctx.backup("def".into(), bup_dir.into(), example_key_file.into());

        // Then
        fs::remove_dir_all(bup_dir).expect("Failed to remove test bup dir");
        assert!(result.is_ok());
    }

    /// Validate that the expected PDF file is generated when calling
    /// `backup()` function.
    #[test]
    fn backup_creates_pdf_with_correct_name_and_path() {
        // Given
        let example_key_file =
            "tests/test_files/example_keys/email_only/example_key.pgp";
        let base_name = "my_test_base_name";
        let bup_dir = "03_backup_dir";
        let expected_pdf_file = format!("{}/{}.pdf", bup_dir, base_name);
        let ctx = BackupCtx {
            backup_chunk_size: 600,
        };

        // When
        let _ = ctx
            .backup(base_name.into(), bup_dir.into(), example_key_file.into())
            .unwrap();

        // Then
        let pdf_exists = Path::new(&expected_pdf_file).exists();
        fs::remove_dir_all(bup_dir).expect("Failed to remove test bup dir");
        assert!(pdf_exists);
    }

    /// Validate that backing up key with multiple subkeys works.
    #[test]
    fn backup_returns_ok_when_backing_up_cert_with_subkeys() {
        // Given
        let example_key_file =
            "tests/test_files/example_keys/email_and_name_with_subkeys/priv_key.pgp".to_string();
        let base_name = "multiple_subkeys".to_string();
        let backup_dir = "04_backup_dir".to_string();
        let ctx = BackupCtx {
            backup_chunk_size: 600,
        };

        // When
        let result = ctx.backup(
            base_name.to_owned(),
            backup_dir.to_owned(),
            example_key_file.to_owned(),
        );

        // Then
        fs::remove_dir_all(backup_dir.to_owned()).expect(
            format!("Failed to remove test result dir: {}", backup_dir)
                .as_str(),
        );
        assert!(result.is_ok());
    }

    /// Verify that successful restore() returns Ok.
    #[test]
    fn restore_email_and_name_key_converted_bup_returns_oks() {
        // Given
        let bup_dir =
            "tests/test_files/restoring/email_and_name/converted/output";
        let priv_key_file = "01_restore.pgp";

        // When
        let result = restore(bup_dir.into(), priv_key_file.into());

        // Then
        fs::remove_file(priv_key_file)
            .expect("Failed to remove the restore result file");
        assert!(result.is_ok());
    }

    /// Verify that successful restore() on a short key with name and email restores the key
    /// correctly.
    #[test]
    fn restore_email_and_name_key_converted_restored_correctly() {
        // Given
        let bup_dir =
            "tests/test_files/restoring/email_and_name/converted/output";
        let orig_cert_path =
            "tests/test_files/example_keys/email_and_name/john.doe.pgp";
        let priv_key_file = "02_restore.pgp";
        let orig_key = Cert::from_file(orig_cert_path).expect(
            format!("Failed to read key from {}", orig_cert_path).as_str(),
        );

        // When
        let _ = restore(bup_dir.into(), priv_key_file.into()).expect(
            format!("Failed to restore to {}", priv_key_file).as_str(),
        );

        // Then
        let recovered_key = Cert::from_file(priv_key_file).expect(
            format!("Failed to load recovered key: {}", priv_key_file)
                .as_str(),
        );
        fs::remove_file(priv_key_file).expect(&format!(
            "Failed to remove the test file: {}",
            priv_key_file
        ));
        assert_eq!(recovered_key, orig_key);
    }

    /// Verify that the order of scanned pages doesn't matter -- restore() restores the key
    /// correctly regardless of the names of the original jpeg file names.
    #[test]
    fn restore_email_and_name_key_converted_unordered_restored_correctly() {
        // Given
        let bup_dir =
            "tests/test_files/restoring/email_and_name/converted_unordered/output";
        let orig_cert_path =
            "tests/test_files/example_keys/email_and_name/john.doe.pgp";
        let priv_key_file = "03_restore.pgp";
        let orig_key = Cert::from_file(orig_cert_path).expect(
            format!("Failed to read key from {}", orig_cert_path).as_str(),
        );

        // When
        let _ = restore(bup_dir.into(), priv_key_file.into()).expect(
            format!("Failed to restore to {}", priv_key_file).as_str(),
        );

        // Then
        let recovered_key = Cert::from_file(priv_key_file).expect(
            format!("Failed to load recovered key: {}", priv_key_file)
                .as_str(),
        );
        fs::remove_file(priv_key_file).expect(
            format!("Failed to remove test file {}", priv_key_file).as_str(),
        );
        assert_eq!(recovered_key, orig_key);
    }

    /// Verify that restoring a key from a converted (not-scanned) set of JPEGs containing
    /// individual pages of the Back-up PDF constructs the expected cert.
    #[test]
    fn restore_many_ids_converted_bup_restores_correctly_long() {
        // Given
        let bup_dir =
            "tests/test_files/restoring/many-ids/converted-from-pdf";
        let priv_key_file = "04_restore.pgp";
        let orig_key = Cert::from_file(
            "tests/test_files/example_keys/many_user_ids_key/cert.pgp",
        )
        .expect("Failed to read a test Cert");

        // When
        let _ = restore(bup_dir.into(), priv_key_file.into());

        // Then
        let restored_key =
            Cert::from_file(priv_key_file).expect("Failed to read test Cert");
        let restored_correctly = restored_key == orig_key;
        fs::remove_file(priv_key_file)
            .expect("Failed to remove the restore result file");
        assert!(restored_correctly)
    }

    /// Validate that restoring a backup of cert with multiple subkeys (authn, sign and encryption)
    /// yields the orig cert.
    #[test]
    fn restore_multiple_subkeys_restore_to_orig_cert() {
        // Given
        let backup_dir = "tests/test_files/restoring/email_and_name_with_subkeys/converted";
        let private_key = "05_restore.pgp";
        let orig_cert_file = "tests/test_files/example_keys/email_and_name_with_subkeys/priv_key.pgp";
        let orig_cert = Cert::from_file(orig_cert_file).expect(
            format!("Failed to read the test cert: {}", orig_cert_file)
                .as_str(),
        );

        // When
        let _ = restore(backup_dir.into(), private_key.into())
            .expect(&format!("Failed to restore to {}", private_key));

        // Then
        let restored_cert = Cert::from_file(private_key)
            .expect("Failed to read the restored cert");
        fs::remove_file(private_key).expect("Failed to remove test cert");
        assert_eq!(restored_cert, orig_cert);
    }

    /// Validate that a large backup (little over 50 pages) of actual scans works correctly
    #[test]
    fn restore_large_scanned_backup_restores_to_orig_cert_long() {
        // Given
        let backup_dir =
            "tests/test_files/restoring/many-ids/scanned-from-pdf";
        let private_key = "06_restore.pgp";
        let orign_cert_file =
            "tests/test_files/example_keys/many_user_ids_key/cert.pgp";
        let orig_cert = Cert::from_file(orign_cert_file).expect(
            format!("Failed to read the test cert: {}", orign_cert_file)
                .as_str(),
        );

        // When
        let _ = restore(backup_dir.into(), private_key.into())
            .expect(&format!("Failed to restore to {}", private_key));

        // Then
        let restored_cert = Cert::from_file(private_key).expect(&format!(
            "Failed to read the restored cert from {}",
            private_key
        ));
        fs::remove_file(private_key).expect(&format!(
            "Failed to remove the test cert: {}",
            private_key
        ));
        assert_eq!(restored_cert, orig_cert);
    }
}
