#[macro_use]
extern crate log;
extern crate pretty_env_logger;

use clap::{Args, Parser, Subcommand};

use crate::backup_restore::{restore, BackupCtx};

mod backup_restore;
mod qr_code;

#[derive(Parser)]
#[command(author, version, about, long_about)]
/// Back-up and restore OpenPGP key in a paper-printable form
///
/// Performs an OpenPGP Private Key back-up in a paper-printable form. It also
/// allows restoring the Private key from a scanned back-up.
///
/// By default, the tool won't print detail log messages to standard output. For
///  troubleshooting, one may run the tool with an environment variable:
/// RUST_LOG=debug. E.g.:
///
/// $ RUST_LOG=debug openpgp-paper-backup ...
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Backs-up an OpenPGP Private Key. It will create a set of Qr Code
    /// files (.jpeg) and a PDF in the back-up directory.
    ///
    /// After creating the back-up, please, scan the PDF file and store it
    /// in a safe place.
    Backup(BackupArgs),

    /// Restores an OpenPGP Private Key.
    ///
    /// In order to restore the Private Key back-up, please, print the scanned
    /// PDF file in a set of .jpeg files and store them in a new directory.
    Restore(RestoreArgs),
}

#[derive(Args)]
struct BackupArgs {
    #[arg(long, value_name = "BASE_NAME")]
    /// Sets a base name for the QrCode files
    ///
    /// For example, if set to my_key, it'll produce files named:
    /// my_key_01.jpeg, my_key_02.jpeg etc.
    base_name: String,

    #[arg(long, value_name = "PATH_TO_PRIVATE_KEY_FILE")]
    /// The OpenPGP Private Key file.
    ///
    /// A full path to the OpenPGP Private Key you're backing-up.
    key_file: String,

    #[arg(long, value_name = "PATH_TO_BACKUP_DIR")]
    /// Back-up directory.
    ///
    /// A full path to the directory in which you've stored the scanned
    /// PDF pages.
    backup_dir: String,
}

#[derive(Args)]
struct RestoreArgs {
    #[arg(long, value_name = "BACKUP_DIR")]
    /// Directory containing '.jpeg' files of the scanned Backup PDF.
    backup_dir: String,

    #[arg(long, value_name = "PRIVATE_KEY_FILE")]
    /// Private Key file to which we're restoring
    ///
    /// For example, if you want to restore to file:
    /// ~/my_private_files/private_key.pgp
    ///
    /// pass the above path as a value of this argument.
    private_key_file: String,
}

fn main() {
    let cli = Cli::parse();

    pretty_env_logger::init();

    match cli.command {
        Commands::Backup(args) => {
            info!(
                "Attempting to back-up private key: {}, to directory: {} \
                with a base name: {}",
                args.key_file, args.backup_dir, args.base_name
            );
            let bup = BackupCtx {
                backup_chunk_size: 600,
            };
            bup.backup(args.base_name, args.backup_dir, args.key_file)
                .expect("Failed to back up the Private Key!")
        }
        Commands::Restore(args) => {
            info!(
                "Here we're attempting to restore from the directory: {} \
                    to private key file: {}",
                args.backup_dir, args.private_key_file
            );
            restore(args.backup_dir, args.private_key_file)
                .expect("Failed to restore the Private Key from Backup")
        }
    }
}
